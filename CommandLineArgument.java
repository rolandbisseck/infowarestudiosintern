/**
 * Created by Packard bell on 9/22/2015.
 */
public class CommandLineArgument
{
        public static void main(String[] args)
        {
            System.out.println("There are " + args.length + " arguments given.");

            String argumentName = "";
            int position = 0;
            /*here i get the name of the argument, its position by using the array string of argument in the main method */
            for(int x = 0; x < args.length; x++)
            {
                argumentName = args[x];
                position = x;
                System.out.println("The argument " + (x + 1) + " is " + argumentName + " and is at position " + position);
            }

        }

}
