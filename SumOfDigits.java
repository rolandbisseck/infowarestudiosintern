import javax.swing.*;

/**
 * Created by Packard bell on 9/17/2015.
 */
public class SumOfDigits
{
    public static void main(String[] args)
    {
        int number = Integer.parseInt(JOptionPane.showInputDialog(null, "enter a number"));
        int sumOfItsDigits = 0;

        while(number > 0)
        {

            sumOfItsDigits = sumOfItsDigits + number % 10;
            number = number / 10;
        }
        System.out.println(sumOfItsDigits);

    }
}
