public class DeleteFileAndDirectory
{
    public static void main(String[] args)
    {
        //i created a file
        File file = new File("C:\\doc\\input.txt");
        File directory = new File("C\\docs");

        boolean delete = file.delete();
        try
        {
            if(delete)
            {
                System.out.println("file  deleted");
            }
            else
            {
                System.out.println("file not deleted");
            }
            if(directory.isDirectory())
            {
                if(directory.list().length == 0)
                {
                    System.out.println("directory successfully deleted");
                }
                else
                {
                    System.out.println("directory not deleted");
                }
            }


        }
        catch(Exception error)
        {
            System.out.println(error.getMessage());
        }

    }
}