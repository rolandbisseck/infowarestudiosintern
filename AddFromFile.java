import java.io.IOException;
import java.io.*;
import java.util.*;
/**
 * Created by Packard bell on 9/21/2015.
 */
public class AddFromFile
{
    public static void main(String[] args)
    {
        int sum = 0;
        File file = new File("C:\\\\Users\\\\Packard bell\\\\Documents\\\\angrams\\\\word.txt");
        try
        {
            /* here i get what is inside the file and put it in the arraylist of string
            where i can access them and populate */
            ArrayList<String> numberList = read(file);

            for(int x = 0; x < numberList.size();x++)
            {
                sum = sum + Integer.parseInt(numberList.get(x));
                System.out.print( " + " + numberList.get(x));
            }
             System.out.print(" = " + sum);
        }
        catch(Exception er)
        {
            er.getMessage();
        }
    }
    //this method will read first is in the file, and return an arraylist of the element in that file.
    public static ArrayList<String> read(File file)throws IOException
    {
        ArrayList<String> details = new ArrayList<String>();
        BufferedReader buffer = new BufferedReader(new FileReader(file));
        String record = "";

        while((record = buffer.readLine()) != null)
        {
            details.add(record);

        }
        buffer.close();
        return details;
    }
    /*this method return the number total of item in that file.
     but i did not implement it because i did not use the array of string
     i rather used the arraylist*/
    public static int totalOfRecord(File file)throws IOException
    {
        int totalNumber = 0;
        BufferedReader buffer = new BufferedReader(new FileReader(file));
        while(buffer.readLine() != null)
        {
            totalNumber = totalNumber + 1;
        }
        buffer.close();
        return totalNumber;
    }
}
