/**
 * Created by Packard bell on 9/22/2015.
 */
import com.sun.deploy.util.ArrayUtil;

import java.util.ArrayList;
public class QuickSort
{
    public static void main(String[] args)
    {


    }
    public static void quickSort(int[] list)
    {
        /*i created 3 arrays
        the first one will contain the numbers that are lesser than the pivot
        the second one will contain the number that is equal to the pivot
        the third one that will contain the numbers that are greater than the pivot*/
        int[] less = new int[0];
        int[] equal = new int[0];
        int[] greater = new int[0];
        int pivot = 0;
        for(int x = 0; x < list.length; x++)
        {
            if(list.length > 1)
            {
                pivot = list[x];
                if(list[x] < pivot)
                {
                    less = new int[x];
                }
                else if(list[x] == pivot)
                {
                    equal = new int[x];
                }
                else if(list[x] > pivot)
                {
                    greater = new int[x];
                }
                quickSort(less);

                // here we concatenate the arrays
                //we first concatenate the less array and the equal and we add the greater array
                int lengthOfArray = less.length + equal.length;
                int[]listEqualLesser = new int[lengthOfArray];
                System.arraycopy(less,0,list,0,less.length);
                System.arraycopy(equal,0,list,equal.length,less.length);

                quickSort(greater);
            }
        }
    }
}
